<?php

  /** ACF Block : download **/

?>

<div class="download <?php if ($download_bg_color ) : echo getContrast50($download_bg_color); ?>"
  style="background: <?php echo $download_bg_color; ?> center center / cover no-repeat<?php endif; ?>">
  <div class="download__inner">
    <?php if ($download_title ) : ?>
      <h3 class="download__title">
        <?php echo $download_title; ?>
      </h3>
    <?php endif;
    if (have_rows('download_list') ) : ?>
      <div class="download__list <?php if ($download_2_col ) { echo 'download__list--two-col';} ?>">
        <?php while (have_rows('download_list') ) : the_row();
          $icon = get_sub_field('download_file-icon');
          $icon_color = get_sub_field('download_icon-color');
          $file = get_sub_field('download_file');
          $filename = get_sub_field('download_file-name');
          $filedesc = get_sub_field('download_file-description');
          $filesize = size_format(filesize(get_attached_file($file['id']))) ? : '';
          if ($file ) :
				?>
				<a class="file" href="<?php echo $file['url']; ?>">
				<div class="file__inner">
				<?php if ($icon ) : ?>
                <div class="file__icon">
                  <i <?php if ($icon_color ) :?> style="color: <?php echo $icon_color; ?>"<?php endif; ?> class="fa <?php echo $icon; ?>" aria-hidden="true"></i>  
                </div>
              <?php endif; ?>
                <div class="file__data">
                  <?php if ($filename ) : ?>
                    <div class="file__name">
                      <?php echo $filename; ?>
                    </div>
                  <?php endif; ?>
                  <div class="file__size">
                    <?php echo $filesize; ?>
                  </div>
                  <?php if ($filedesc ) : ?>
                    <div class="file__desc">
                      <?php echo $filedesc; ?>
                    </div>
                  <?php endif; ?>
                </div>
				</div>
				</a>
				<?php endif;
        endwhile;
      endif; ?>
    </div>
  </div>
</div>