<?php

namespace Nodopiano\Steroids\Download;

use Nodopiano\Steroids\BlockInterface;

class Download implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('download_boxed'),
            'grid_content' => get_sub_field('download_grid-content'),
            'bg_color' => get_sub_field('download_bg-color'),
            'bg_color_opacity' => get_sub_field('download_bg-color-opacity'),
            'bg_img' => get_sub_field('download_bg-img'),
            'bg_content' => get_sub_field('download_bg-color-content'),
            'bg_content_opacity' => get_sub_field('download_bg-color-content-opacity'),
            'title' => get_sub_field('download_title'),
            'two_col' => get_sub_field('download_2-col'),
            'list' => get_sub_field('download_list')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
