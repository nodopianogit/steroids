<?php

/** ACF Block : slideshow **/

?>

<div class="slideshow <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit';} ?>" role="region" aria-label="<?php echo get_the_title(); ?>"
  <?php if ($app->get('slideshow_engine') == 'orbit' ) : ?>
    data-orbit data-options="
      animInFromLeft:<?php echo $app->get('slideshow_anim_left_in'); ?>; animInFromRight:<?php echo $app->get('slideshow_anim_right_in'); ?>;
      animOutToLeft:<?php echo $app->get('slideshow_anim_left_out'); ?>; animOutToRight:<?php echo $app->get('slideshow_anim_right_out'); ?>;"
  <?php endif; ?>
>
  <ul class="slideshow__inner <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-container';} ?>">
    <button class="slideshow__prev <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-previous';} ?>">
      <span class="show-for-sr"><?php echo __('Slide precedente', $site_domain); ?></span>&#9664;&#xFE0E;
    </button>
    <button class="slideshow__next <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-next';} ?>">
      <span class="show-for-sr"><?php echo __('Slide successiva', $site_domain); ?></span>&#9654;&#xFE0E;
    </button>
    <?php if (have_rows('slide') ) : $i = 0;
      while (have_rows('slide') ) : the_row(); $i++;
			$slide_bg_color = (get_sub_field('slide_bg-color') ? get_sub_field('slide_bg-color') : 'transparent');
			$slide_caption_title = get_sub_field('slide_caption-title');
			$slide_caption_txt = get_sub_field('slide_caption-txt');
			$slide_media = get_sub_field('slide_media'); ?>

															<li class="slideshow__slide <?php if ($i == 1 ) { echo 'is-active';} if ($app->get('slideshow_engine') == 'orbit' ) { echo ' orbit-slide';} ?>" style="background: <?php echo $slide_bg_color; ?>;">
															<?php switch ($slide_media ) {

            case 'immagine':

																											$slide_img = get_sub_field('slide_img');
																											$slide_img_url = $slide_img['sizes']['np-xlarge'];
																			  ?>
																						<img class="slideshow__img <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-image';} ?>" src="<?php echo $slide_img_url; ?>" alt="<?php echo $slide_img['alt']; ?>"
																						data-interchange="
																						[<?php echo $slide_img['sizes']['np-small']; ?>, small],
																						[<?php echo $slide_img['sizes']['np-medium']; ?>, medium],
																						[<?php echo $slide_img['sizes']['np-large']; ?>, large],
																						[<?php echo $slide_img_url; ?>, xxlarge]">
																						<figcaption class="slideshow__caption <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-caption';} ?>">
																						  <h4 class="caption__title"><?php echo $slide_caption_title; ?></h4>
																						  <div class="caption__txt">
																					<?php echo $slide_caption_txt; ?>
																						  </div>
																						</figcaption>
																						  <?php
            break;

            case 'file-video':

																											  $video_poster = get_sub_field('video_poster');
																			  ?>
																					<div class="responsive-embed">
																					  <?php if ( ! is_handheld() ) : ?>
                    <video class="video" autoplay loop muted poster="<?php echo $video_poster['sizes']['np-large']; ?>">
                      <?php if (have_rows('slide_file-video') ) :
                        while (have_rows('slide_file-video') ) : the_row();
								  $video_source = get_sub_field('video');
								  $video_ext = strrpos($video_source, '.') + 1;
								  $video_format = substr($video_source, $video_ext); ?>

																						  <source src="<?php echo $video_source; ?>" type="video/<?php echo $video_format; ?>">
																						<?php endwhile;
                      endif; ?>
                    </video>
                  <?php else : ?>
                    <img class="slideshow__img" src="<?php echo $video_poster['sizes']['np-large']; ?>" alt="<?php echo $video_poster['alt']; ?>"
                      data-interchange="
                      [<?php echo $video_poster['sizes']['np-small']; ?>, small],
                      [<?php echo $video_poster['sizes']['np-medium']; ?>, medium],
                      [<?php echo $video_poster['sizes']['np-large']; ?>, large],
                      [<?php echo $video_poster; ?>, xxlarge]"/>
                  <?php endif; ?>
								<figcaption class="slideshow__caption <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-caption';} ?>">
								  <h4 class="caption__title"><?php echo $slide_caption_title; ?></h4>
								  <div class="caption__txt">
									<?php echo $slide_caption_txt; ?>
								  </div>
								</figcaption>
									  </div>

								  <?php break;

            case 'streaming-video':
																											  $video = get_sub_field('slide_streaming-video');
																											  $video_poster = get_sub_field('video_poster');
																			  ?>

																						  <div class="responsive-embed streaming-video">
																					<?php if ( ! is_handheld() ) : ?>
                  <div id="video-<?php echo $video; ?>" class="video"></div>
                <?php else : ?>
                  <img class="slideshow__img" src="<?php echo $video_poster['sizes']['np-large']; ?>" alt="<?php echo $video_poster['alt']; ?>"
                    data-interchange="
                    [<?php echo $video_poster['sizes']['np-small']; ?>, small],
                    [<?php echo $video_poster['sizes']['np-medium']; ?>, medium],
                    [<?php echo $video_poster['sizes']['np-large']; ?>, large],
                    [<?php echo $video_poster; ?>, xxlarge]"/>
                <?php endif; ?>
																					<figcaption class="slideshow__caption <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-caption';} ?>">
																					  <h4 class="caption__title"><?php echo $slide_caption_title; ?></h4>
																					  <div class="caption__txt">
																						<?php echo $slide_caption_txt; ?>
																					  </div>
																					</figcaption>
																						  </div>

																						<?php break;
																}// End switch().
;?>    
																	</li>
																	<?php endwhile;
    endif; ?>

     <script type="text/javascript">
      if ($('.streaming-video').length > 0) {
        var tag = document.createElement('script');
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        function onYouTubeIframeAPIReady() {

          <?php if (have_rows('slide') ) : $i = 0;
            while (have_rows('slide') ) : the_row(); $i++;
					  $slide_media = get_sub_field('slide_media');
					  $video = get_sub_field('slide_streaming-video');
					  $video_start = get_sub_field('video_start');
					  $video_end = get_sub_field('video_end');
					  if ($slide_media == 'streaming-video' ) : ?>
						video_streaming('<?php echo $video; ?>', <?php echo $video_start; ?>, <?php echo $video_end; ?>);
					  <?php endif;
            endwhile;
          endif; ?>

        }
      }
    </script>
  </ul>
  <nav class="slideshow__dots <?php if ($app->get('slideshow_engine') == 'orbit' ) { echo 'orbit-bullets';} ?>">
    <?php if (have_rows('slide') ) : $i = 0;
      while (have_rows('slide') ) : the_row(); $i++; ?>
														<button class="<?php if ($i == 0 ) { echo 'is-active'; } ?>" data-slide="<?php echo $i; ?>">
														<span class="show-for-sr"><?php echo $slide_caption_title; ?></span>
														<?php if ($i == 0 ) : ?><span class="show-for-sr"><?php echo __('Slide attuale', $site_domain); ?></span><?php endif; ?>
														</button>
														<?php endwhile;
    endif; ?>
  </nav>
</div>
