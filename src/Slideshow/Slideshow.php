<?php
namespace Nodopiano\Steroids\Slideshow;

use Nodopiano\Steroids\BlockInterface;

class Slideshow implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'title' => get_the_title(),
            'boxed' => get_sub_field('slideshow_boxed'),
            'grid_content' => get_sub_field('slideshow_grid-content'),
            'bg_color' => get_sub_field('slideshow_bg-color'),
            'height' => get_sub_field('slideshow_height'),
            'slide' => get_sub_field('slide')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
