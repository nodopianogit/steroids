<?php

namespace Nodopiano\Steroids\Content;

use Nodopiano\Steroids\BlockInterface;

class Content implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'content' => get_the_content(),
            'boxed' => get_sub_field('contenuto-standard_boxed'),
            'grid_content' => get_sub_field('contenuto-standard_grid-content'),
            'bg_color' => get_sub_field('contenuto-standard_bg-color'),
            'bg_color_opacity' => get_sub_field('contenuto-standard_bg-color-opacity'),
            'bg_img' => get_sub_field('contenuto-standard_bg-img'),
            'bg_content' => get_sub_field('contenuto-standard_bg-color-content'),
            'bg_content_opacity' => get_sub_field('contenuto-standard_bg-color-content-opacity')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
