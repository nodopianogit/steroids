<?php
  /*
  * ACF Block : content
  * @package Nodopiano
  */
?>
<div class="np-knot<?php if ( $data['boxed'] ) { echo '--boxed';} ?> contenuto-standard <?php if ( $data['bg_color'] ) { echo getContrast50( $data['bg_color'] );} ?>"
  <?php if ( $data['bg_color'] ) : ?>
    <?php if ( $data['bg_color_opacity'] ) : ?>
      style="background-color: rgba(<?php echo hexToRgb( $data['bg_color'] ) ?>, <?php echo $data['bg_color_opacity']; ?>"
    <?php else : ?>
      style="background-color: <?php echo $data['bg_color']; ?>"
    <?php endif; ?>
  <?php endif; ?>
  <?php if ( $data['bg_img'] ) : ?>
    data-interchange="
    [<?php echo $data['bg_img']['sizes']['np-small']; ?>, small],
    [<?php echo $data['bg_img']['sizes']['np-medium']; ?>, medium],
    [<?php echo $data['bg_img']['sizes']['np-large']; ?>, large],
    [<?php echo $data['bg_img']['sizes']['np-xlarge']; ?>, xlarge]"
  <?php endif; ?>
>
  <div class="contenuto-standard__inner np-knot__inner<?php if ( $data['grid_content'] ) { echo '--boxed';} ?> <?php if ( $data['bg_content'] ) { echo getContrast50( $data['bg_content'] );} ?>"
    <?php if ( $data['bg_content'] ) : ?>
      <?php if ( $data['bg_content_opacity'] ) : ?>
        style="background-color: rgba(<?php echo hexToRgb( $data['bg_content'] ) ?>, <?php echo $data['bg_content_opacity']; ?>"
      <?php else : ?>
        style="background-color: <?php echo $data['bg_content']; ?>"
      <?php endif; ?>
    <?php endif; ?>
  >
    <?php echo $data['content']; ?>
  </div>
</div>
