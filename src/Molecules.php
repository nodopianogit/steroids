<?php
/**
* Steroids 1.0.0
* Molecules
*
* @package Nodopiano
* @since 1.0.0
*/


namespace Nodopiano\Steroids;

class Molecules
{
    protected $block;
    protected $class;
    protected $dir;

    public function __construct($block, $dir = null)
    {
        $this->block = str_replace('-', '', ucfirst($block));
        $this->dir = $dir;

        if (file_exists($dir . '/steroids/' . $this->block . '/' . $this->block . '.php')) {
            $class = '\\Nodopiano\\Steroids\\Custom\\' . $this->block . '\\' . $this->block ;
        } else {
            $class = '\\Nodopiano\\Steroids\\' . $this->block . '\\' . $this->block ;
        }
        $this->class = new $class();
    }

    public function getData()
    {
       return array_merge($this->class->data(), ['index' => rand(1,100000)] );
    }

    public function synthesize()
    {
        global $app;
        $payload = [
                'app' => $app->toArray(),
                'data' => $this->getData(),
            ];

        if (file_exists($this->dir . '/steroids/' . $this->block . '/' . $this->block . '.html')) {
            \Timber\Timber::render('shape.html', $payload);
            \Timber\Timber::$locations = $this->dir . '/steroids/' . $this->block . '/';
            \Timber\Timber::render($this->block . '.html', $payload);
        } else {
            \Timber\Timber::render('shape.html', $payload);
            \Timber\Timber::render($this->block . '/' . $this->block . '.html', $payload);
        }
    }
}
