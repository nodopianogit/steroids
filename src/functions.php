<?php

    global $app;
    $app = new Nodopiano\Steroids\Substances();

// Custom image sizes
if (function_exists('add_image_size')) {
  add_image_size('np-square', 640, 640, true);
  add_image_size('np-square-large', 1200, 1200, true);

  add_image_size('np-small', 640);
  add_image_size('np-medium', 1024);
  add_image_size('np-large', 1200);
  add_image_size('np-xlarge', 1920);
}

  /** Mobble Plugin! */
  if (function_exists('the_field') && !function_exists('is_handheld')) {
      add_action('admin_notices', 'mobble_notice');
  }

  function mobble_notice()
  {
      ?>
    <div class="error notice">
        <p>
          <?php echo __('Se hai inserito dei video, devi installare il plugin '); ?>
          <b><a href="https://it.wordpress.org/plugins/mobble/" target="_blank">Mobble</a></b>
          <?php echo __(' affinché i controlli sul dispositivo funzionino. Vai alla pagina dei '); ?>
          <b><a href="/wp-admin/plugins.php"><?php echo __('plugins'); ?></a></b>
        </p>
    </div>
  <?php

  }

  /** Configurazione option pages */
  if (function_exists('acf_add_options_page')) {
      acf_add_options_page([
      'page_title' => 'Opzioni',
      'menu_title' => 'Opzioni',
      'menu_slug' => 'opzioni',
      'capability' => 'edit_posts',
      'redirect' => true,
      ]);

    // Add option page
    acf_add_options_sub_page([
      'page_title' => 'Dati generali',
      'menu_title' => 'Dati generali',
      'parent_slug' => 'opzioni',
      ]);

      acf_add_options_sub_page([
      'page_title' => 'Footer',
      'menu_title' => 'Footer',
      'parent_slug' => 'opzioni',
      ]);
  }
  if (function_exists('add_filter')){
  add_filter('acf/settings/google_api_key', function () {
      global $app;
      return $app->get('google_maps_api_key');
  });
}

  /** Contrasto dinamico testo */
  function getContrast50($hex_color)
  {
      return (hexdec($hex_color) > 0xffffff / 1.15) ? 'color--dark' : 'color--light';
  }

  function getContrastSvg($hex_color)
  {
      return (hexdec($hex_color) > 0xffffff / 2) ? 'color--dark' : 'color--light';
  }

  /** Conversione colore da HEX a RGB */
  function hexToRgb($hex)
  {
      $hex = str_replace('#', '', $hex);

      if (strlen($hex) == 3) {
          $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
          $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
          $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
      } else {
          $r = hexdec(substr($hex, 0, 2));
          $g = hexdec(substr($hex, 2, 2));
          $b = hexdec(substr($hex, 4, 2));
      }
      $rgb = "$r, $g, $b";

      return $rgb;
  }
