<?php

namespace Nodopiano\Steroids\Loop;

use Nodopiano\Steroids\BlockInterface;

class Loop implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $loop_categories = [];
        if (get_sub_field('loop_categories')) :
          foreach (get_sub_field('loop_categories') as $post_cat) :
            array_push($loop_categories, $post_cat->term_id);
        endforeach;
        endif;

        $loop_args = [
            'post_type' => get_sub_field('loop_post-type'),
            'post_status' => 'publish',
            'cat' => $loop_categories ?: '',
            'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
            'posts_per_page' => get_sub_field('loop_qta') ?: -1,
            'orderby' => get_sub_field('loop_order-by'),
            'order' => get_sub_field('loop_order') ? 'ASC' : 'DESC'
        ];

        $this->data = [
            'boxed' => get_sub_field('loop_boxed'),
            'grid_content' => get_sub_field('loop_grid-content'),
            'bg_color' => get_sub_field('loop_bg-color'),
            'bg_color_opacity' => get_sub_field('loop_bg-color-opacity'),
            'bg_img' => get_sub_field('loop_bg-img'),
            'post_type' => get_sub_field('loop_post-type'),
            'categories' => get_sub_field('loop_categories'),
            'qta' => get_sub_field('loop_qta'),
            'title' => get_sub_field('loop_title'),
            'txt' => get_sub_field('loop_txt'),
            'order_by' => get_sub_field('loop_order-by'),
            'order' => get_sub_field('loop_order'),
            'date' => get_sub_field('loop_date'),
            'btn_txt' => get_sub_field('loop_txt-btn'),
            'page_label' => get_sub_field('loop_page-label'),
            'page_link' => get_sub_field('loop_page-link'),
            'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
            'posts' => \Timber\Timber::get_posts($loop_args),
        ];
        // $this->data['posts'] = new WP_Query($loop_args);
        $this->data['pagination'] = \Timber\Timber::get_pagination();
    }

    public function data()
    {
        return $this->data;
    }
}
