<?php

  /** ACF Block : rassegna-post **/

?>

<div class="rassegna-post <?php if ($rassegna_post_bg_color ) : echo getContrast50($rassegna_post_bg_color); ?>" style="background: <?php echo $rassegna_post_bg_color; endif; ?>">
  <div class="rassegna-post__inner">
    <h3 class="rassegna-post__title">
      <?php echo $rassegna_post_title; ?>
    </h3>
    <div class="rassegna-post__text">
      <?php echo $rassegna_post_txt; ?>
    </div>
    <?php
      $rassegna_categories = array();
      if ($rassegna_post_categories ) :
        foreach ($rassegna_post_categories as $post_cat ) :
          array_push($rassegna_categories, $post_cat->term_id);
        endforeach;
      endif;
      $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
      $rassegna_post_args = array(
        'post_type' => $rassegna_post_type,
        'post_status' => 'publish',
        'cat' => $rassegna_categories ? $rassegna_categories : '',
        'posts_per_page' => $rassegna_post_qta ?: -1,
        'paged' => $paged,
        'orderby' => 'date',
        'order' => 'DESC',
      );
      $rassegna_post = new WP_Query($rassegna_post_args);
      if ($rassegna_post->have_posts() ) :
        while ($rassegna_post->have_posts() ) : $rassegna_post->the_post();
			  $categories = wp_get_post_terms(get_the_ID(), array('category'));
			  ?>
				<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post'; endforeach; ?>">
				<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__inner'; endforeach; ?>">
				<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__title'; endforeach; ?>">
                <a href="<?php the_permalink(); ?>">
                  <h4><?php the_title(); ?></h4>
                </a>
				</div>
				<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__excerpt'; endforeach; ?>">
				<?php the_excerpt(); ?>
				</div>
				<?php if ($rassegna_post_date ) : ?>
                <span class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__date'; endforeach; ?>"><?php echo get_the_date(); ?></span>
              <?php endif; ?>
				<?php if ($categories ) :
                foreach ($categories as $cat ) : ?>
													   | <span class="post__cat"><a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo $cat->name; ?></a></span>
													<?php endforeach;
					endif;
                $featured = get_the_post_thumbnail_url();
                if ($featured ) :
  $featured_alt = get_post_meta($featured->ID, '_wp_attachment_image_alt', true);
?>
<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__featured'; endforeach; ?>">
<img data-interchange="
[<?php echo get_the_post_thumbnail_url(null, 'featured-small'); ?>, small],
[<?php echo get_the_post_thumbnail_url(null, 'featured-medium'); ?>, medium],
[<?php echo get_the_post_thumbnail_url(null, 'featured-large'); ?>, large]"
src="<?php echo get_the_post_thumbnail_url(null, 'featured-large'); ?>" width="1024" height="768" alt="<?php echo $featured_alt; ?>"/>
</div>
<?php endif; ?>
				<div class="<?php foreach ($rassegna_post_type as $post_type ) : echo $post_type . '-post__btn'; endforeach; ?>">
                <a class="button" href="<?php the_permalink(); ?>"><?php echo $rassegna_post_btn_txt; ?></a>
			  </div>
			</div>
		  </div>
		<?php endwhile; ?>
      <?php
        if ($news_eventi->max_num_pages > 1 ) :
        $prev_page = 'pagina ' . intval($paged + 1) . ' >>';
        $next_page = '<< pagina ' . intval($paged -1);
      ?>
        <nav class="prev-next-posts">
          <div class="prev-posts-link">
            <?php if (get_previous_posts_link( $next_page ) ) : ?>
              <button class="button"><?php echo get_previous_posts_link( $next_page ); ?></button>
            <?php endif; ?>
          </div>
          <div class="next-posts-link">
            <?php if (get_next_posts_link( $prev_page, $news_eventi->max_num_pages ) ) : ?>
              <button class="button"><?php echo get_next_posts_link( $prev_page, $news_eventi->max_num_pages ); ?></button>
            <?php endif; ?>
          </div>
        </nav>
      <?php endif; ?>
    <?php endif;
    wp_reset_postdata();
    if ($rassegna_post_label ) : ?>
									  <div class="rassegna-post__btn">
									<a class="rassegna-post__link" href="<?php echo $rassegna_post_link; ?>"><?php echo $rassegna_post_label; ?></a>
									  </div>
									<?php endif; ?>
  </div>
</div>