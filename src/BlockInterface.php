<?php
namespace Nodopiano\Steroids;

interface BlockInterface
{
    public function data();
}
