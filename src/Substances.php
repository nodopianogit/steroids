<?php

namespace Nodopiano\Steroids;

/**
 * Author: Nodopiano
 * URL: https://www.nodopiano.it
 *
 * Nodopiano functions and definitions for Steroids
 */
  class Substances
  {
      protected $settings = [
                'site_domain' => 'playground',
                'breakpoints' => [
                    'origin' => 0,
                    'small' => 600,
                    'medium' => 900,
                    'large' => 1200,
                    'xlarge' => 1600,
                    'xxlarge' => 2000,
                ],
                'responsive_images' => [
                    'origin' => 600,
                    'small' => 900,
                    'medium' => 1200,
                    'large' => 1600,
                    'xlarge' => 2160,
                    'xxlarge' => 2500,
                ],
                'txt_img' => [
                    'origin' => 600,
                    'small' => 600,
                    'medium' => 640,
                    'large' => 800,
                    'xlarge' => 960,
                    'xxlarge' => 1024,
                ],
                'cards' => [
                    'origin' => 300,
                    'small' => 300,
                    'medium' => 300,
                    'large' => 512,
                    'xlarge' => 512,
                    'xxlarge' => 512,
                ],
                'slideshow_engine' => 'orbit',
                'slideshow_timing' => 8000,
                'slideshow_anim_left_in' => 'fade-in',
                'slideshow_anim_left_out' => 'fade-out',
                'slideshow_anim_right_in' => 'fade-in',
                'slideshow_anim_right_out' => 'fade-out',
                'gallery_engine' => '', // '' or 'swipebox' - Leave empty for Foundation's Reveal
                'popup_size' => 'full', // 'tiny', 'small', 'large', 'full'
                'popup_animation_in' => 'scale-in-up', // Reveal animation IN
                'popup_animation_out' => 'scale-out-down', // Reveal animation OUT
                'partners_engine' => 'slick', // Leave empty for no effects
                'google_maps_api_key' => ''
            ];

      public function __construct($settings = [])
      {
          $this->settings = array_merge($this->settings, $settings);
      }

      public function get($key)
      {
          return $this->settings[$key];
      }

      public function __get($key)
      {
          return $this->settings[$key];
      }

      public function set($key, $value)
      {
          $this->settings[$key] = $value;
      }

      public function toArray()
      {
          return $this->settings;
      }

      public function __toString()
      {
          return serialize($this->settings);
      }
  }
