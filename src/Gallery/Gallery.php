<?php

namespace Nodopiano\Steroids\Gallery;

use Nodopiano\Steroids\BlockInterface;

class Gallery implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('gallery_boxed'),
            'grid_content' => get_sub_field('gallery_grid-content'),
            'bg_color' => get_sub_field('gallery_bg-color'),
            'bg_img' => get_sub_field('gallery_bg-img'),
            'title' => get_sub_field('gallery_title'),
            'txt' => get_sub_field('gallery_txt'),
            'bg_caption' => get_sub_field('gallery_bg-caption'),
            'bg_caption_opacity' => get_sub_field('gallery_bg-caption-opacity'),
            'img_per_row' => get_sub_field('gallery_img-per-row'),
						'img' => get_sub_field('gallery_img'),
						'index' => rand(1,2000000),
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
