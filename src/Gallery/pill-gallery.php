<?php
  /** ACF Block : gallery
   *
   * @package Nodopiano
   * @since Digital Innovation Hub 1.0.0
   */

?>
<div class="gallery">
  <div class="gallery__inner <?php if ( $gallery_bg_color ) : echo getContrast50( $gallery_bg_color ); endif; if ( $gallery_bg_img ) : echo ' bg-img'; endif; ?>"
    <?php if ( $gallery_bg_color ) : ?>
      style="background: <?php echo $gallery_bg_color; ?> center center / cover no-repeat"
    <?php endif; ?>
    <?php if ( $gallery_bg_img ) : ?>
      data-interchange="
        [<?php echo $gallery_bg_img['sizes']['fp-small']; ?>, small],
        [<?php echo $gallery_bg_img['sizes']['fp-medium']; ?>, medium],
        [<?php echo $gallery_bg_img['sizes']['fp-large']; ?>, large],
        [<?php echo $gallery_bg_img['sizes']['fp-xlarge'] ?>;]"
    <?php endif; ?>
  >
    <?php if ( $gallery_title ) : ?>
      <div class="gallery__title">
        <h4><?php echo $gallery_title; ?></h4>
      </div>
    <?php endif; ?>
    <?php if ( $gallery_txt ) : ?>
      <div class="gallery__text">
        <?php echo $gallery_txt; ?>  
      </div>
    <?php endif; ?>
    <?php
      switch ( $gallery_img_per_row ) {
        case 'tre':
          $number = 'three-per-row';
        break;
        case 'quattro':
          $number = 'four-per-row';
        break;
        case 'cinque':
          $number = 'five-per-row';
        break;
        default:
          $number = 'four-per-row';
        break;
      }
      if ( $gallery_img ) : ?>
        <ul class="gallery__images no-bullet <?php echo $number; ?>">
          <?php foreach ( $gallery_img as $image ) : ?>
            <li class="gallery__image g-image">
              <a class="swipebox" rel="<?php echo $gallery_obj['key']; ?>" href="<?php echo $image['url']; ?>">
                <img src="<?php echo $image['sizes']['fp-small']; ?>" alt="<?php echo $image['alt']; ?>" />
                <div class="g-image__caption <?php if ( $gallery_bg_caption ) : echo getContrast50( $gallery_bg_caption ); endif; ?>"
                  <?php if ( $gallery_bg_caption ) : ?>
                    style="background: <?php echo $gallery_bg_caption; ?> center center / cover no-repeat"
                  <?php endif; ?>
                >
                  <p class="caption__inner"><?php echo $image['caption']; ?></p>
                </div>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
    <?php endif; ?>
  </div>
</div>