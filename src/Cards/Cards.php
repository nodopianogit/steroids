<?php

namespace Nodopiano\Steroids\Cards;

use Nodopiano\Steroids\BlockInterface;

class Cards implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('cards_boxed'),
            'grid_content' => get_sub_field('cards_grid-content'),
            'bg_color' => get_sub_field('cards_bg-color'),
            'bg_color_opacity' => get_sub_field('cards_bg-color-opacity'),
            'bg_img' => get_sub_field('cards_bg-img'),
            'title' => get_sub_field('cards_title'),
            'card' => get_sub_field('card')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
