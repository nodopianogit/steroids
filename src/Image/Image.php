<?php

namespace Nodopiano\Steroids\Image;

use Nodopiano\Steroids\BlockInterface;
use Timber\Timber;

class Image implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('image_boxed'),
            'grid_content' => get_sub_field('image_grid-content'),
            'parallax' => get_sub_field('image_parallax'),
            'full' => get_sub_field('image_full-width'),
            'gif' => get_sub_field('gif_animata'),
            'txt' => get_sub_field('image_txt'),
            'img' => get_sub_field('image_img'),
            'size' => 'np-xlarge'
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
