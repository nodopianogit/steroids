<?php

  /** ACF Block : immagine **/

?>

<div class="image <?php if ($image_parallax ) { echo 'parallax ';} if ($image_full ) { echo 'full-width ';} ?>">
  <div class="image__inner">
    <?php if ($image_txt ) : ?>
      <div class="image__txt">
        <?php echo $image_txt; ?>
      </div>
    <?php endif; ?>
    <div class="image__img">
      <img <?php if ( ! $image_gif ) : ?>
        data-interchange="
          [<?php echo $image_img['sizes']['fp-small']; ?>, small],
          [<?php echo $image_img['sizes']['fp-medium']; ?>, medium],
          [<?php echo $image_img['sizes']['fp-large']; ?>, large],
          [<?php echo $image_img['sizes'][ $image_size ]; ?>, xxlarge]"
          src="<?php echo $image_img['sizes'][ $image_size ]; ?>"
          width="<?php echo $image_img['sizes'][ $image_size . '-width' ]; ?>"
          height="<?php echo $image_img['sizes'][ $image_size . '-height' ]; ?>"
        <?php else : ?>
          src="<?php echo $image_img['url'] ?>"
          width="1920px" height="1080px" 
        <?php endif; ?>
        alt="<?php echo $image_img['alt']; ?>"/>
    </div>
  </div>
</div>