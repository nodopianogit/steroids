<?php

namespace Nodopiano\Steroids\Options;

use Nodopiano\Steroids\BlockInterface;

class Options implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'ragione_sociale' => get_field('data_ragione-sociale', 'option'),
            'sedi' => get_field('data_sedi', 'option'),
            'recapiti_telefonici' => get_field('data_recapiti-telefonici', 'option'),
            'indirizzi_email' => get_field('data_indirizzi-email', 'option'),
            'cf_piva' => get_field('data_cod-fisc-iva', 'option'),
            'cod_fisc' => get_field('data_codice-fiscale', 'option'),
            'p_iva' => get_field('data_partita-iva', 'option'),
            'capitale_sociale' => get_field('data_cap-soc', 'option'),
            'numero_rea' => get_field('data_rea', 'option'),
            'altri_dati' => get_field('data_various', 'option'),
            'partners_title' => get_field('partners_title', 'option'),
            'partners_txt' => get_field('partners_txt', 'option'),
            'partners' => get_field('partners', 'option'),
            'social' => get_field('social', 'option'),
            'privacy_policy' => get_field('privacy_policy', 'option'),
            'cookie_policy' => get_field('cookie_policy', 'option'),
            'credits' => get_field('opzioni_credits', 'option')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
