<?php
  /*
  * ACF Block : txt
  * @package Nodopiano
  */
?>
<div class="np-knot<?php if ( $data['boxed'] ) { echo '--boxed'; } ?> txt <?php if ( $data['bg_color'] ) { echo getContrast50( $data['bg_color'] );} ?>"
  <?php if ( $data['bg_color'] ) : ?>
    <?php if ( $data['bg_color_opacity'] ) : ?>
      style="background-color: rgba(<?php echo hexToRgb( $data['bg_color'] ) ?>, <?php echo $data['bg_color_opacity']; ?>"
    <?php else : ?>
      style="background-color: <?php echo $data['bg_color']; ?>"
    <?php endif; ?>
  <?php endif; ?>
  <?php if ( $data['bg_img'] ) : ?>
    data-interchange="
      [<?php echo $data['bg_img']['sizes']['np-small']; ?>, small],
      [<?php echo $data['bg_img']['sizes']['np-medium']; ?>, medium],
      [<?php echo $data['bg_img']['sizes']['np-large']; ?>, large],
      [<?php echo $data['bg_img']['sizes']['np-xlarge']; ?>, xxlarge]"
  <?php endif; ?>
>
  <div class="np-knot__inner<?php if ( $data['grid_content'] ) { echo '--boxed'; } ?> txt__inner <?php if ( $data['bg_content'] ) { echo getContrast50( $data['bg_content'] );} ?>"
    <?php if ( $data['bg_content'] ) : ?>
      <?php if ( $data['bg_content_opacity'] ) : ?>
        style="background-color: rgba(<?php echo hexToRgb( $data['bg_content'] ) ?>, <?php echo $data['bg_content_opacity']; ?>"
      <?php else : ?>
        style="background-color: <?php echo $data['bg_content']; ?>"
      <?php endif; ?>
    <?php endif; ?>
  >
    <?php if ( $data['title'] ) : ?>
      <h3 class="txt__title">
        <?php echo $data['title']; ?>
      </h3>
    <?php endif; ?>
    <?php if ( $data['two_col'] ) : ?>
      <div class="txt__col">
        <?php echo $data['col_sx']; ?>
      </div>
      <div class="txt__col">
        <?php echo $data['col_dx']; ?>
      </div>
    <?php else : ?>
      <div class="txt__text">
        <?php echo $data['col_sx']; ?>
      </div>
    <?php endif; ?>
  </div>
</div>