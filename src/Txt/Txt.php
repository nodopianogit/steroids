<?php

namespace Nodopiano\Steroids\Txt;

use Nodopiano\Steroids\BlockInterface;

class Txt implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('txt_boxed'),
            'bg_color' => get_sub_field('txt_bg-color'),
            'bg_color_opacity' => get_sub_field('txt_bg-color-opacity'),
            'bg_img' => get_sub_field('txt_bg-img'),
            'grid_content' => get_sub_field('txt_grid-content'),
            'bg_content' => get_sub_field('txt_bg-color-content'),
            'bg_content_opacity' => get_sub_field('txt_bg-color-content-opacity'),
            'title' => get_sub_field('txt_title'),
            'catalog' => get_sub_field('txt_catalog'),
            'two_col' => get_sub_field('txt_2-col'),
            'col_sx' => get_sub_field('txt_col-sx'),
            'col_dx' => get_sub_field('txt_col-dx')
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
