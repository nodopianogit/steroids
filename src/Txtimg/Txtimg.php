<?php

namespace Nodopiano\Steroids\Txtimg;

use Nodopiano\Steroids\BlockInterface;
use Timber\Timber;

class Txtimg implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            'boxed' => get_sub_field('txt-img_boxed'),
            'bg_color' => get_sub_field('txt-img_bg-color'),
            'bg_color_opacity' => get_sub_field('txt-img_bg-color-opacity'),
            'bg_img' => get_sub_field('txt-img_bg-img'),
            'grid_content' => get_sub_field('txt-img_grid-content'),
            'bg_content' => get_sub_field('txt-img_bg-color-content'),
            'bg_content_opacity' => get_sub_field('txt-img_bg-color-content-opacity'),
            'title' => get_sub_field('txt-img_title'),
            'catalog' => get_sub_field('txt-img_catalog'),
            'reverse' => get_sub_field('txt-img_reverse'),
            'col_sx' => get_sub_field('txt-img_txt'),
            'img_loop' => get_sub_field('txt-img_img'),
            'image_size' => 'np-square-large'
        ];
    }

    public function data()
    {
        return $this->data;
    }
}
