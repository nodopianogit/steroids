<?php
  /*
  * ACF Block : txt-img
  * @package Nodopiano
  */
?>
<div class="np-knot<?php if ( $data['boxed'] ) { echo '--boxed'; } ?> txt-img <?php if ( $data['reverse'] ) { echo 'invertito ';} if ( $data['bg_color'] ) { echo getContrast50( $data['bg_color'] );} ?>"
  <?php if ( $data['bg_color'] ) : ?>
    <?php if ( $data['bg_color_opacity'] ) : ?>
      style="background-color: rgba(<?php echo hexToRgb( $data['bg_color'] ) ?>, <?php echo $data['bg_color_opacity']; ?>"
    <?php else : ?>
      style="background-color: <?php echo $data['bg_color']; ?>"
    <?php endif; ?>
  <?php endif; ?>
  <?php if ( $data['bg_img'] ) : ?>
    data-interchange="
    [<?php echo $data['bg_img']['sizes']['np-small']; ?>, small],
    [<?php echo $data['bg_img']['sizes']['np-medium']; ?>, medium],
    [<?php echo $data['bg_img']['sizes']['np-large']; ?>, large],
    [<?php echo $data['bg_img']['sizes']['np-xlarge']; ?>, xxlarge]"
  <?php endif; ?>
>
  <div class="np-knot__inner<?php if ( $data['grid_content'] ) { echo '--boxed'; } ?> txt-img__inner <?php if ( $data['bg_content'] ) { echo getContrast50( $data['bg_content'] );} ?>"
    <?php if ( $data['bg_content'] ) : ?>
      <?php if ( $data['bg_content_opacity'] ) : ?>
        style="background-color: rgba(<?php echo hexToRgb( $data['bg_content'] ) ?>, <?php echo $data['bg_content_opacity']; ?>"
      <?php else : ?>
        style="background-color: <?php echo $data['bg_content']; ?>"
      <?php endif; ?>
    <?php endif; ?>
  >
    <?php if ( $data['title'] ) : ?>
      <h3 class="txt-img__title">
        <?php echo $data['title']; ?>
      </h3>
    <?php endif; ?>
    <?php if ( $data['col_sx'] ) : ?>
      <div class="txt-img__text txt-img__col">
        <?php echo $data['col_sx']; ?>
      </div>
    <?php endif; ?>
    <?php if ( have_rows( 'txt-img_img' ) ) : ?>
      <div class="txt-img__img txt-img__col">
        <?php
          $rows = get_sub_field( 'txt-img_img' );
          $images = count($rows);
          while ( have_rows('txt-img_img') ) : the_row();
            $image = get_sub_field( 'img' );
  		  ?>
  			    <div class="img__inner <?php if ( $images > 1 ) { echo 'multiple'; } ?>">
  			      <img data-interchange="
  			        [<?php echo $image['sizes']['np-square']; ?>, small],
  		          [<?php echo $image['sizes']['np-square']; ?>, medium],
  	            [<?php echo $image['sizes'][ $data['image_size'] ]; ?>, large],
                [<?php echo $image['sizes'][ $data['image_size'] ]; ?>, xxlarge]"
  			        src="<?php echo $image['sizes'][ $data['image_size'] ]; ?>"
  			        width="<?php echo $image['sizes'][ $data['image_size'] . '-width' ]; ?>"
  			        height="<?php echo $image['sizes'][ $data['image_size'] . '-height' ]; ?>"
  		          alt="<?php echo $image['alt']; ?>"/>
  		      </div>
  			<?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</div>