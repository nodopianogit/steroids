# Steroids

Custom ACF Layouts library for websites

## About Timber 2 compatibility
To avoid problems with PHP 7.4, you should use Twig at version 2, which requires Timber 2 as well (currently unstable).

Steroids "dev-master" does not support Timber 2 in order to maintain retro-compatibility.

You can install a special Steroids version that supports Timber 2 using the following command:
```sh   
composer require nodopiano/steroids 2.0.0-alpha
```

## Getting Started

Steroids is a custom ACF Layouts library which gives extra functionality to your WordPress theme, just like steroids.
Every ACF Layout is called **Pill**.

Right now, these are the **Pills** available in Steroids:

1. *Contenuto standard* - A field that summons the default content of the page, like the function ``` get_the_content(); ``` does.
    These are its options:
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color.
    - *Immagine di sfondo* - Allows to apply a background image to the layout.
    - *Colore di sfondo del contenuto* - Allows to apply a background color to the layout's content
    - *Opacità di sfondo del contenuto* - Allows to apply an opacity value to the selected color.

2. **Testo** - A Wysiwyg field with
    - *Catalogazione* - Allows to insert a character near the layout title.
    - *Titolo* - Allows to insert a title for the layout.
    - *Contenuto su 2 colonne* - Allows to split the layout's content in two columns (another wysiwyg will appear).
    - *Testo* - Allows to insert a text (if *Contenuto su 2 colonne* is selected, this will be the left column).
    - *Testo colonna DX* - Allows to insert the text in the right column (only if *Contenuto su 2 colonne* is selected).
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color.
    - *Immagine di sfondo* - Allows to apply a background image to the layout.
    - *Colore di sfondo del contenuto* - Allows to apply a background color to the layout's content
    - *Opacità di sfondo del contenuto* - Allows to apply an opacity value to the selected color.

3. **Testo e immagine** - A Wysiwyg / Image field with
    - *Catalogazione* - Allows to insert a character near the layout title.
    - *Titolo* - Allows to insert a title for the layout.
    - *Inverti le colonne* - Allows to switch the order of the columns.
    - *Testo* - Allows to insert a text
    - *Immagine* - Allows to insert from 1 to 4 images (default in the right column).
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color.
    - *Immagine di sfondo* - Allows to apply a background image to the layout.
    - *Colore di sfondo del contenuto* - Allows to apply a background color to the layout's content
    - *Opacità di sfondo del contenuto* - Allows to apply an opacity value to the selected color.

4. **Immagine** - An Image field with
    - *Immagine* - Allows to insert an image.
    - *Testo* - Allows to insert a text over the image.
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Effetto Parallax* - Allows to add parallax effect to image on page scrolling.
    - *GIF Animata* - Allows to insert an animated GIF as image (this will avoid wordpress to crop the uploaded image).

5. **Loop**
    - *Titolo* - Allows to insert a title for the layout.
    - *Testo* - Allows to insert a text
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color.
    - *Immagine di sfondo* - Allows to apply a background image to the layout.
    - *Custom Post Type* - Allows to select the **Custom Post Type** of the posts to loop, it gives you also the capability to add a new one on the fly.
    - *Categorie* - Allows to select the **categories** of the posts to loop (multiple checkboxes selection)
    - *Quantità* - Allows to choose the number of max posts to show in loop (if you have more posts to show, this will enable **pagination**)
    - *Ordinamento* - Allows to choose the **orderBy** method (Es. Order by Date, Order by Title, Order by menu_order)
    if you want to order posts manually for example using the order number in posts page, we suggest you to install [Simple Page Ordering](https://it.wordpress.org/plugins/simple-page-ordering/ "Get Simple Page Ordering") WordPress Plugin, which allows you to order your posts with a simple Drag&Drop.
    - *Ordine* - Allows to choose the **order** method (Es. ASC or DESC)
    - *Mostra la data* - Allows to hide/show the post date.
    - *Testo bottone* - Allows to insert a text for the *Read more* button that links to the relative's post page.
    - *Link Pagina Madre* - Allows to select the page that lists all your posts if you had one.
    - *Testo link Pagina Madre* - If you already have a page that lists all your posts (Es. News or Events), this option allows you to insert a text for the button that links to that page.

6. **Slideshow** - A Repeater of Image fields with
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Altezza* - Allows to choose if the slideshow has to be full-height tall or not according to the viewport.
    - *Slide* - The slide of the slideshow, it is a repeater with:
        - *Colore di sfondo* - Allows to apply a background color to the slide (Useful if you want a slide with a unique color as background)
        - *Titolo* - Allows to insert a title for the slide.
        - *Testo* - Allows to insert a text for the slide.
        - *Elemento media* - Allows to choose the type of media you want to insert in your slide:
            - *Color* if you want to use the background color
            - *Immagine* if you want to use an image
            - *Video da file* if you want to insert a video from a file (Es. like .webm, .mp4, .ogg)
            - *Video Streaming* if you want to insert a video in streaming from a file (Es. like YouTube or Vimeo)
        - *Elemento media* - Allows to choose the type of media you want to insert in your slide:
        - *Immagine* - Allows to insert an image.
        - *Video streaming* - Allows to insert the URL of a streaming video.
        - *Video da file* - Allows to insert the file videos (Es. for cross browser compatibility we suggest you to upload *.webm*, *.mp4* extensions).
        - *Video Poster* - Allows to choose an image as fallback in case something in the loading of videos goes wrong (Es. on mobile devices videos are replaced with this image).
        - *Inizio del video* - Allows to insert the value in seconds which determines the beginning of the video
        - *Fine del video* - Allows to insert the value in seconds which determines the ending of the video

7. **Galleria Immagini** - A Gallery field with
    - *Titolo* - Allows to insert a title for the layout.
    - *Testo* - Allows to insert a text
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color
    - *Immagine di sfondo* - Allows to apply a background image to the layout
    - *Immagini per riga* - Allows to select how many images to display per row, it will work only on screen that are wider than 1200px.
    - *Colore di sfondo della didascalia* - Allows to apply a background color to the image's caption
    - *Immagini* - Allows to select gallery's images

8. **Lista file scaricabili** - A Repeater field with
    - *Titolo* - Allows to insert a title for the layout.
    - *Contenuto su 2 colonne* - Allows to split the layout's content in two columns
    - *Boxato* - Allows to force layout width to stay fit in grid (default is 75rem)
    - *Contenuto in griglia* - Allows to force layout's content width to stay fit in grid (default is 75rem)
    - *Colore di sfondo del blocco* - Allows to apply a background color to the layout
    - *Opacità di sfondo del blocco* - Allows to apply an opacity value to the selected color
    - *Immagine di sfondo* - Allows to apply a background image to the layout
    - *Colore di sfondo del contenuto* - Allows to apply a background color to the layout's content
    - *Opacità di sfondo del contenuto* - Allows to apply an opacity value to the selected color.
    - *File* - The file block, it is a repeater with:
        - *Forza download al click* - Allows to add 'download' attribute to the anchor (Download directly).
        - *Icona* - Allows to write the name of a web font icon (Useful if you use FontAwesome)
        - *Colore dell'icona* - Allows to select a font color for the icon
        - *Nome* - Allows to insert the file name.
        - *Descrizione* - Allows to insert the file description.
        - *File* - Allows to upload your file

Steroids will also create an *Option Page* with 2 child pages:
- **Dati generali** - Allows you to insert your business's data (Es. Headquarters, email addresses, telephone numbers etc.)
- **Footer** - Allows you to insert a carousel with your Partners & also boring data (Es. Policies, Credits etc.)

### Prerequisites

ACF (Advanced Custom Fields PRO) (WordPress Plugin)
Advanced Custom Fields is a WordPress plugin which allows you to add extra content fields to your WordPress edit screens.
[https://www.advancedcustomfields.com/pro](https://www.advancedcustomfields.com/pro/ "Get ACF PRO")

Timber (WordPress Plugin)
Plugin to write WordPress themes w object-oriented code and the Twig Template Engine
[Plugin - https://it.wordpress.org/plugins/timber-library/](https://it.wordpress.org/plugins/timber-library/ "Get Timber")
[Docs - https://timber.github.io/docs](https://timber.github.io/docs/ "Timber Documentation")

Mobble (WordPress Plugin) (If you're going to add **Slideshow** Pill)
Mobble provides mobile related conditional functions for your site and also add device information to the body class of your theme allowing you to easily target your CSS for different gadgets.
[Plugin - https://it.wordpress.org/plugins/mobble/](https://it.wordpress.org/plugins/mobble/ "Get Mobble")

If you choose 'Slick' as Slideshow engine, you need to install [Slick Carousel](http://kenwheeler.github.io/slick/ "Get Slick Carousel"):
```
# NPM
npm install slick-carousel
```

### Installing

To clone Steroids on your project, go to your WordPress theme folder and type:

```
composer config repositories.steroids git git@gitlab.com:nodopianogit/steroids.git
composer require nodopiano/steroids:dev-master
```

### Configuration

include the required files in your function.php
```
require_once 'vendor/autoload.php';
require_once 'vendor/nodopiano/steroids/class-substances.php';
```
### Usage

in your pages, add this chunk of code inside your loop and you're ready to go ;)

```
<?php 
if (have_rows('builder')) :
    $index = 0;
    while (have_rows('builder')) : the_row(); $index++;
    	$block = get_row_layout();
    	$molecule = new Nodopiano\Steroids\Molecules($block, dirname(__FILE__), $settings);
    	$molecule->synthesize();
	endwhile;
endif;
?>
```

### Customize
Do you want to syntesize a new Molecule or overwrite an existing one?
```
mkdir -p steroids/YourMoleculeName
```
and update your composer.json file with this
```
  "autoload": {
      "psr-4": {
        "Nodopiano\\Steroids\\Custom\\" : "steroids/"
      }
    },
```
```
composer dump-autoload
```
inside your new directory create a PHP class like this

```
<?php

namespace Nodopiano\Steroids\Custom\YourMoleculeName;

use Nodopiano\Steroids\BlockInterface;

class YourMoleculeName implements BlockInterface
{
    protected $data;

    public function __construct()
    {
        $this->data = [
            // get acf data here 
        ];
    }

    public function data()
    {
        return $this->data;
    }
}

```
and a twig HTML file named YourMoleculeName.html

## Authors

[**Nodopiano**](https://www.nodopiano.it)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Have to add **Fast Links** Pill to Steroids
* Have to improve posts pagination in **Loop**
